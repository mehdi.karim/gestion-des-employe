package com.example.gestion.employe.repositories;

import com.example.gestion.employe.entities.EmployeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeRepository extends CrudRepository<EmployeEntity, Long> {

}
