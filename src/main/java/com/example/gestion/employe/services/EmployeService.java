package com.example.gestion.employe.services;

import com.example.gestion.employe.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public interface EmployeService {

    UserDto createEmploye(UserDto userDto);
}
