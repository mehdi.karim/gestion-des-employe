package com.example.gestion.employe.services;

import com.example.gestion.employe.dto.UserDto;
import com.example.gestion.employe.entities.EmployeEntity;
import com.example.gestion.employe.repositories.EmployeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeServiceImpl implements EmployeService {

    @Autowired
    EmployeRepository employeRepository;

    @Override
    public UserDto createEmploye(UserDto user) {

        EmployeEntity employeEntity = new EmployeEntity();

        BeanUtils.copyProperties(user, employeEntity);

        employeEntity.setEncryptedPassword("test password");

        employeEntity.setUserId("test user id");

        EmployeEntity newEmployer = employeRepository.save(employeEntity);

        UserDto userDto = new UserDto();

        BeanUtils.copyProperties(newEmployer, userDto);

        return userDto;
    }
}
