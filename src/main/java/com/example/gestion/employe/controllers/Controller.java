package com.example.gestion.employe.controllers;

import com.example.gestion.employe.dto.UserDto;
import com.example.gestion.employe.requestes.EmployeRequest;
import com.example.gestion.employe.responses.EmployeResponse;
import com.example.gestion.employe.services.EmployeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employers")
public class Controller {

    @Autowired
    EmployeService employeService;

    @GetMapping
    public String getEmploye(){
        return "get employe";
    }

    @PostMapping
    public EmployeResponse createEmploye(@RequestBody EmployeRequest employeRequest){

        UserDto userDto = new UserDto();

        BeanUtils.copyProperties(employeRequest, userDto);

        UserDto createEmploye = employeService.createEmploye(userDto);

        EmployeResponse employeResponse = new EmployeResponse(); //return user enregistre

        BeanUtils.copyProperties(createEmploye, employeResponse);

        return employeResponse;
    }

    @PutMapping
    public String updateEmploye(){
        return "update was called";
    }

    @DeleteMapping
    public String deleteEmploye(){
        return "delete was called";
    }


}
