package com.example.gestion.employe.requestes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeRequest {

    private String firstName;
    private String lastName;
    private String age;
    private String title;
    private String email;
    private String password;
}
